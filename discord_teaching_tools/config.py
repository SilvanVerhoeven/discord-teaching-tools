import json
import os

__location__ = os.path.realpath(
    os.path.join(os.getcwd(), os.path.dirname(__file__)))
with open(os.path.join(__location__, "config.json"), encoding="utf-8") as config_file:
    config = json.load(config_file)

# Maximum length of a discord nickname
MAX_NICKNAME_LENGTH = config["MAX_NICKNAME_LENGTH"] \
    if "MAX_NICKNAME_LENGTH" in config else 32

# This language will be used per default, every user can change his language
DEFAULT_LOCALE = config["DEFAULT_LOCALE"] \
    if "DEFAULT_LOCALE" in config else "en"

# Relative directory to locale-directory
LOCALE_DIR = os.path.join(__location__, config["LOCALE_DIR"]) \
    if "LOCALE_DIR" in config else os.path.join(__location__, "locales")

# Special commands for /raise [command]
RAISE_SPECIALS = {None: "🖐"}
RAISE_SPECIALS.update(config["RAISE_SPECIALS"]
                      if "RAISE_SPECIALS" in config else {
                            "yes": "✅",
                            "no": "❌",
                            "clap": "👏",
                            "ok": "👍",
                            "?": "❓",
                            "question": "❓",
                            "help": "🆘"
                        })

# Location of data-log of servers the bot is member of
DATA_PATH_SERVERS = os.path.join(__location__, config["DATA_PATH_SERVERS"]) \
    if "DATA_PATH_SERVERS" in config else os.path.join(__location__, "data", "servers.json")
