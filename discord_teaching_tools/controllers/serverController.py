from ..config import DATA_PATH_SERVERS
from ..components.server import Server
import json
import io


class ServerController:
    """Administrates all servers the bot is member of and all server-related functions."""

    def __init__(self):
        self._servers = {}

    def __del__(self):
        self._servers = None

    def _save_servers_to_file(self):
        """Writes all currently controlled servers into data file."""
        with io.open(DATA_PATH_SERVERS, mode="w", encoding="utf-8") as servers_file:
            servers_file.write(
                json.dumps({"server_ids": [_server_id for _server_id in self._servers]}))

    def _load_servers_from_file(self):
        """Loads all servers from data file and overwrites the controlled servers."""
        with io.open(DATA_PATH_SERVERS, mode="r", encoding="utf-8") as servers_file:
            server_ids = json.loads(servers_file.read())["server_ids"]
            self._servers = {_server_id: Server(_server_id) for _server_id in server_ids}

    def _add(self, server):
        """Adds given server to controlled servers and returns instance of controlled server."""
        if self.is_known(server):
            return
        self._servers[server.id] = Server(server.id)
        self._save_servers_to_file()
        return self._servers[server.id]

    def is_known(self, server):
        """Returns True if given server is already controlled, otherwise False."""
        return server.id in self._servers

    def handle(self, server):
        if server is None or server.id is None:
            raise ValueError(f'Invalid server: {server}')
        self._add(server)
        self._load_servers_from_file()

    def handle_message(self, message):
        self.handle(message.author.guild)
