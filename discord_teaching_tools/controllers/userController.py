from ..components.user import User
from .baseController import BaseController


class UserController(BaseController):
    def __init__(self,  bot):
        super().__init__(bot)
        self._users = []

    def __del__(self):
        self._users = None

    def _add(self, user):
        """Adds given user to controlled users if not known yet and returns instance of the controlled user."""
        if self.is_known(user):
            return self._get_by_id(user.id)
        self._users.append(User(user))
        return self._users[len(self._users)-1]

    async def handle(self, message):
        """Adds message author to controlled users if it is a human user
        and user is not known already."""
        if not self.is_user_command(message):
            return
        self._add(message.author)

    def is_known(self, user):
        """Returns True if given user is already controlled."""
        return len([_user for _user in self._users if user.id == _user.id]) > 0

    def get(self, user):
        """Returns controlled user of given user, adds user to controlled users if not known yet."""
        if user is None:
            raise ValueError("User cannot be none")
        return self._add(user)

    def _get_by_id(self, user_id):
        if user_id is None:
            raise ValueError("UserID cannot be none")
        return next((_user for _user in self._users if user_id == _user.id), None)

    def remove(self, user):
        if user is None:
            raise ValueError("User cannot be none")
        return self.remove_by_id(user.id)

    def remove_by_id(self, user_id):
        if user_id is None:
            raise ValueError("UserID cannot be none")
        self._users = [_user for _user in self._users if not user_id == _user.id]

    def print_all(self):
        """Prints list of user IDs of all controlled users."""
        for _user in self._users:
            print(_user.id)

