from abc import abstractmethod
from .baseController import BaseController
from ..config import DEFAULT_LOCALE


class BaseTranslationController(BaseController):
    def __init__(self, bot, user_controller):
        super().__init__(bot)
        self._users = user_controller
        self.locale = DEFAULT_LOCALE

    @abstractmethod
    async def _handle_message(self, message):
        pass

    async def handle(self, message):
        controlled_user = self._users.get(message.author)
        self.locale = controlled_user.locale if controlled_user is not None else DEFAULT_LOCALE
        await self._handle_message(message)
