from .baseTranslationController import BaseTranslationController
from ..components.stickyMessage import create_sticky_message
from ..translation import _


class StickyMessagesController(BaseTranslationController):
    def __init__(self, bot, user_controller):
        super().__init__(bot, user_controller)
        self.bot = bot
        self.sticky_messages = {}

    def __del__(self):
        self.bot = None

    async def _handle_message(self, message):
        """Handles message, executes commands if necessary.
        Returns True if a command was executed, False otherwise."""

        if message is None:
            return False

        payload = super().get_command_payload(message.content)

        if super().equals_user_command(message, _("/detach", self.locale)):
            self._detach_sticky_message_in(message.channel)
            return True
        elif super().equals_user_command(message, _("/remove", self.locale)):
            await self._remove_sticky_message_in(message.channel)
            return True
        elif super().equals_user_command(message, _("/stick", self.locale)):
            await self._stick_content_in(message.channel, payload)
            return True
        else:
            await self._reposition_sticky_message(message)

        return False

    def _channel_has_sticky_message(self, channel):
        return (channel is not None) and \
               (channel.id in self.sticky_messages) and \
               (self.sticky_messages[channel.id] is not None)

    async def _stick_content_in(self, channel, content):
        """Create a sticky message on given channel with given content."""
        if self._channel_has_sticky_message(channel):
            await self.sticky_messages[channel.id].update(content)
            return
        self.sticky_messages[channel.id] = await create_sticky_message(channel, content)

    def _detach_sticky_message_in(self, channel):
        """Detaches sticky message in given channel, so it will be pushed up
        by newer messages again."""
        if not self._channel_has_sticky_message(channel):
            return
        del self.sticky_messages[channel.id]

    async def _remove_sticky_message_in(self, channel):
        """Deletes sticky message in given channel."""
        if not self._channel_has_sticky_message(channel):
            return
        await self.sticky_messages[channel.id].remove()
        del self.sticky_messages[channel.id]

    async def _reposition_sticky_message(self, message):
        """Repositions given message in its channel to be at
        the very bottom of the channel again."""
        if (message is None or
            self.is_bot_command(message)) or \
           (not self._channel_has_sticky_message(message.channel)):
            return
        await self.sticky_messages[message.channel.id].reposition()
