import discord
from .baseTranslationController import BaseTranslationController
from ..config import MAX_NICKNAME_LENGTH, RAISE_SPECIALS
from ..translation import _


class RaiseHandsController(BaseTranslationController):
    def __init__(self, bot, user_controller):
        super().__init__(bot, user_controller)
        self.bot = bot
        self.original_usernames = {}

    def __del__(self):
        self.bot = None

    async def _handle_message(self, message):
        """Handles message, executes commands if necessary.
        Returns True if a command was executed, False otherwise."""

        if message is None:
            return False

        payload = super().get_command_payload(message.content)

        if super().is_user_command(message) and payload == _("cmds", self.locale):
            await self.print_specials(message.channel)
        elif super().equals_user_command(message, _("/raise", self.locale)):
            await self._raise_hand(message.author, payload)
            return True
        elif super().equals_user_command(message, _("/lower", self.locale)):
            await self._lower_hand(message.author)
            return True

        return False

    async def print_specials(self, channel):
        """Prints all available raise specials in given channel."""

        if channel is None:
            return

        content = _("!raise_specials_overview", self.locale)
        for cmd in RAISE_SPECIALS:
            content += f"**{RAISE_SPECIALS[cmd]}**        {_(f'{cmd}', self.locale)}\n"

        await channel.send(content=content)

    async def _raise_hand(self, user, prefix=None):
        if user is None:
            return
        prefix = RAISE_SPECIALS.get(prefix, prefix)
        if self._get_raised_hand_username(user) is None:
            self._set_raised_hand_username(user, user.display_name)
        try:
            new_username = (prefix + " " + self._get_raised_hand_username(user)
                            )[:MAX_NICKNAME_LENGTH]
            await user.edit(nick=new_username)
        except discord.Forbidden:
            print("Forbidden Raise")
        except:
            self._set_raised_hand_username(user, None)

    async def _lower_hand(self, user):
        if (user is None) or (self._get_raised_hand_username(user) is None):
            return
        try:
            await user.edit(nick=self._get_raised_hand_username(user))
            self._set_raised_hand_username(user, None)
        except discord.Forbidden:
            print("Forbidden")

    def _get_raised_hand_username(self, user):
        return self.original_usernames[user.id] \
            if user.id in self.original_usernames else None

    def _set_raised_hand_username(self, user, value):
        self.original_usernames[user.id] = value
