from .baseTranslationController import BaseTranslationController
from .raiseHandsController import RaiseHandsController
from .stickyMessagesController import StickyMessagesController
from ..translation import _, is_language_available, get_available_languages


class CommandController(BaseTranslationController):
    def __init__(self, bot, user_controller):
        super().__init__(bot, user_controller)
        self.raiseHand = RaiseHandsController(self.bot, user_controller)
        self.stickMessage = StickyMessagesController(self.bot, user_controller)

    async def _handle_message(self, message):
        """Handles given message. Called by self.handle"""

        if self.equals_user_command(message, _("/help", self.locale)):
            await self.print_help(message.channel)
        elif await self.stickMessage.handle(message):
            pass
        elif await self.raiseHand.handle(message):
            pass
        elif self.equals_user_command(message, _("/speak", self.locale)):
            await self.handle_setting_language(message)

    async def handle_setting_language(self, message):
        """Sets language of message author to locale given in command."""
        locale = self.get_command_payload(message.content)
        await self._users.handle(message)
        if locale is None:
            await self.print_languages(message.channel)
            return
        if not is_language_available(locale):
            raise NotImplementedError("Message to author that local is not available")
        self._users.get(message.author).set_language(locale)

    async def print_help(self, channel):
        await channel.send(content=_("!help_message", self.locale))

    async def print_languages(self, channel):
        content = _("!languages_overview", self.locale)
        languages = get_available_languages()
        for locale in languages:
            content += f"**{locale}**      {_(f'{languages[locale]}', self.locale)}\n"
        await channel.send(content=content)
