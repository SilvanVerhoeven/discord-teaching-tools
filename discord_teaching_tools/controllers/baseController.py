from abc import ABC, abstractmethod


class BaseController(ABC):
    def __init__(self, bot):
        self.bot = bot

    def __del__(self):
        self.bot = None

    @staticmethod
    def get_command_payload(message_content):
        """Returns payload of command given in message_content.
        Returns None if message_content doesn't contain a command or
        doesn't contain any payload after the command."""
        if not message_content.startswith("/"):
            return None
        else:
            return message_content.split(" ", 1)[1].strip() \
                if len(message_content.split(" ", 1)) == 2 else None

    def is_user_command(self, message):
        """Returns True if message was sent by a human discord user,
        otherwise False."""
        return not (message is None or
                    self.is_bot_command(message) or
                    message.is_system())

    def is_bot_command(self, message):
        """Returns True if message was sent by this bot,
        otherwise False."""
        return (message is not None) and (message.author == self.bot.user)

    def equals_user_command(self, message, command):
        """Returns True if message contains the given command and
        was sent by a user, otherwise False."""
        if not self.is_user_command(message):
            return False
        return (message.content + " ").startswith(command + " ")

    @abstractmethod
    async def handle(self, message):
        """General entry point for the controller.
        From here, the controller should execute all commands
        applicable to the given message."""
        pass
