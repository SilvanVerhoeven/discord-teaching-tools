import os
import discord
from dotenv import load_dotenv
from .controllers.commandController import CommandController
from .controllers.userController import UserController
from .controllers.serverController import ServerController
from .translation import _


load_dotenv()
TOKEN = os.getenv('DISCORD_TOKEN')

bot = discord.Client()
users = UserController(bot)
serverHandler = ServerController()
commandHandler = CommandController(bot, users)


@bot.event
async def on_ready():
    print(_("!user_greeting", user=bot.user))


@bot.event
async def on_message(message):
    serverHandler.handle_message(message)
    await users.handle(message)
    await commandHandler.handle(message)


def run():
    bot.run(TOKEN)
