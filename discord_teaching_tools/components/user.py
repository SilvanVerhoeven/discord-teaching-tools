from ..config import DEFAULT_LOCALE
from ..translation import is_language_available


class User:
    def __init__(self, user):
        self.id = user.id
        self.display_name = user.display_name
        self.locale = DEFAULT_LOCALE

    def __del__(self):
        self.id = None
        self.display_name = None
        self.locale = None

    def get_language(self):
        """Returns user locale if language with that locale is available, otherwise None."""
        if not is_language_available(self.locale):
            return None
        return self.locale

    def set_language(self, locale):
        """Sets language of user to the language specified by given locale.
        Raises ValueError if locale doesn't match a language."""
        if not is_language_available(locale):
            raise ValueError(f'Language with given locale not available: {locale}')
        self.locale = locale
