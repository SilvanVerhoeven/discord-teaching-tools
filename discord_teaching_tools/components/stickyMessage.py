import discord


async def create_sticky_message(channel, message_content=None):
    """Create a StickyMessage."""
    sticky_message = StickyMessage(channel)

    await sticky_message.update(message_content)
    return sticky_message


class StickyMessage:
    """
    Represents a message modifiable, perpetually at the bottom of a channel staying message.#
    Do not instanciate class directly. Use create_sticky_message instead.
    """

    def __init__(self, channel):
        self.channel = channel
        self.message = None

    def __del__(self):
        del self.channel
        del self.message

    async def _send_message(self, message):
        if message is None:
            return
        await self._send(message.content)
        # TODO: send other stuff of message, like reactions

    async def _send(self, content):
        if content is None:
            return
        await self.remove()
        try:
            self.message = await self.channel.send(content=content)
        except discord.Forbidden:
            print("[ERR] Forbidden: send sticky message")

    async def remove(self):
        """Removes a possible sticky message."""

        if self.message is None:
            return

        try:
            await self.message.delete()
        except discord.Forbidden:
            print("[ERR] Forbidden: delete sticky message")
        except discord.NotFound:
            print("[ERR] NotFound: sticky message")
        self.message = None

    async def update(self, content):
        """
        Replaces message's content with given content.
        Displays message if message had no content before.
        Deletes message in chat if content is None.
        """

        if content is None:
            await self.remove()
        elif self.message is None:
            await self._send(content)
        else:
            try:
                await self.remove()
                await self._send(content)
            except discord.Forbidden:
                print("[ERR] Forbidden: edit sticky message")

    async def reposition(self):
        """Repositions message to be at the very bottom of the channel again."""
        if self.message is None:
            return
        await self._send_message(self.message)
