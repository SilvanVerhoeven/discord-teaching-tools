import i18n
import json
import io
from os import listdir
from os.path import isfile, join
from .config import LOCALE_DIR, DEFAULT_LOCALE


i18n.set('filename_format', '{locale}.{format}')
i18n.set('file_format', 'json')
i18n.load_path.append(LOCALE_DIR)


def _(key, locale=DEFAULT_LOCALE, **kwargs):
    i18n.set('locale', locale)
    return i18n.t(key, **kwargs)


def _get_language_filenames():
    return [filename for filename in listdir(LOCALE_DIR)
            if isfile(join(LOCALE_DIR, filename))]


def _get_locale_from_filename(filename):
    """
    Returns locale from given filename.
    :param filename: Expects filename in given format: {namespace}.{locale}.{file_format}
    :return: locale of given filename
    """
    name_elements = filename.split('.')
    if not len(name_elements) == 3:
        raise ValueError(f'Filename doesn\'t match requirements: '
                         f'Expected format {{namespace}}.{{locale}}.{{file_format}}, given: {filename}')
    return name_elements[1]


def get_available_languages():
    """Returns dict of available languages: locale -> Language Name"""
    filenames = _get_language_filenames()
    languages = {}
    for filename in filenames:
        with io.open(join(LOCALE_DIR, filename), mode="r", encoding="utf-8") as language_file:
            languages[_get_locale_from_filename(filename)] = json.loads(language_file.read())["language"]
    return languages


def is_language_available(locale):
    """Returns True if translation with given locale is available, False otherwise."""
    if locale is None or locale == "":
        return False
    available_locales = [_get_locale_from_filename(filename) for filename in _get_language_filenames()]
    return locale in available_locales
